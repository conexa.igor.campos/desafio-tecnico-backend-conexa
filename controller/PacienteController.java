package com.example.paciente.controller;


import com.example.paciente.model.Paciente;
import com.example.paciente.service.PacienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping(value = "/api/paciente", produces = MediaType.APPLICATION_JSON_VALUE)
public class PacienteController {

    @Autowired
    private PacienteService pacienteService;

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Object> save(@RequestBody Paciente paciente){
        return ResponseEntity.ok(pacienteService.save(paciente));
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<Object> findById(@PathVariable("id") Integer id){
        return ResponseEntity.ok(pacienteService.findById(id));
    }

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<Object> findAll(){
        return ResponseEntity.ok().body(pacienteService.findAll());
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> delete(@PathVariable("id") Integer id){
        pacienteService.delete(id);
        return ResponseEntity.ok().build();
    }

}
