package Checklogin.java;
import java.awt.List;
import java.awt.PageAttributes.MediaType;
import java.lang.annotation.Target;
import java.net.CacheResponse;

import com.example.paciente.repository.PacienteRepository;

public class Médico {
    private int idUsuario;
    private String usuario;
    private String senha;

    public int getIdUsuario() {
        return idUsuario;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }
}
