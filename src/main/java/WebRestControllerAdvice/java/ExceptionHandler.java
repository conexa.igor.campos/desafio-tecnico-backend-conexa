package WebRestControllerAdvice.java;

public @interface ExceptionHandler {

	Class<CustomNotFoundException> value();

}
