package com.example.paciente.controller;


import com.example.paciente.model.Paciente;
import com.example.paciente.service.PacienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;



@RestController
//@RequestMapping(value = "/api/paciente", produces = MediaType.APPLICATION_JSON_VALUE)
public class PacienteController {

    @Autowired
    private PacienteService pacienteService;

    //@RequestMapping(method = RequestMethod.POST)
    public ResponseEntity save(@RequestBody Paciente paciente){
        return ResponseEntity.ok(pacienteService.save(paciente));
    }

    //@RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity findById(@PathVariable("id") Integer id){
        return ResponseEntity.ok(pacienteService.findById(id));
    }

    //@RequestMapping(method = RequestMethod.GET)
    public Object findAll(){
    Object paciente = findAll();
	return ((Object) ResponseEntity.accepted()).equals(paciente);
    }

    //@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable("id") Integer id){
        pacienteService.delete(id);
       
    }

}
