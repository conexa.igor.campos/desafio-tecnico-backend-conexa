package com.example.paciente.controller;

public @interface PathVariable {

	String value();

}
