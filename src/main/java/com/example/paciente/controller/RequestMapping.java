package com.example.paciente.controller;

public @interface RequestMapping {

	String value();

	String produces();

	String method();

}
