package com.example.paciente.exception;



import com.example.paciente.model.Paciente;
import com.example.paciente.repository.PacienteRepository;
import com.example.paciente.service.InternalServerErrorException;
import com.example.paciente.service.UnprocessableEntityException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;



import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;




@Service
public class ExceptionHandlerController {

    private static final Object ListPaciente = null;
	@Autowired
    private PacienteRepository repository;

    public List<Paciente> findAll() throws com.example.paciente.service.UnprocessableEntityException, com.example.paciente.service.InternalServerErrorException{

        try {
            List<Paciente> listPaciente = (List<Paciente>) repository.findAll();

            if(listPaciente.isEmpty()){
                throw new UnprocessableEntityException("Nenhum paciente encontrado");
            }

            listPaciente.stream().map(PacienteResponse::converter).collect(Collectors.toList());

            List<Paciente> listPacienteResponse = null;
			return listPacienteResponse;

        } catch (UnprocessableEntityException e) {
            throw new UnprocessableEntityException(e.getMessage(), e);

        } catch (Exception e) {
            throw new InternalServerErrorException(e.getMessage(), e);
        }
    }

    public PacienteResponse findUserById(Integer id) throws UnprocessableEntityException, InternalServerErrorException{

        try {
            Optional<Paciente> findById = repository.findById(id);

            if(!findById.isPresent()){
                throw new UnprocessableEntityException("Nenhum paciente encontrado", null);
            }

            PacienteResponse pacienteResponse = PacienteResponse.converter(findById.get());

            return pacienteResponse;

        }catch (UnprocessableEntityException e) {
            throw new UnprocessableEntityException(e.getMessage(), e);

        } catch (Exception e) {
            throw new InternalServerErrorException(e.getMessage(), e);
        }
    
    
        

    }

	

	public static Object getListpaciente() {
		return ListPaciente;
	}
}


