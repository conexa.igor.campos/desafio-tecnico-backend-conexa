package com.fluig.config;

public @interface EnableGlobalMethodSecurity {

	boolean prePostEnabled();

}
