package com.fluig.config;

import org.springframework.context.annotation.Configuration;


@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class GlobalMethodSecurityConfiguration {
}