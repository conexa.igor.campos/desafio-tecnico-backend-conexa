package resourceNotFoundException.java;

public @interface ExceptionHandler {

	Class<Exception> value();

}
