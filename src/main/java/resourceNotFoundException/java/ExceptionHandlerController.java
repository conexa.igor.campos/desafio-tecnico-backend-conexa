package resourceNotFoundException.java;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.servlet.ModelAndView;

@ControllerAdvice
 public class ExceptionHandlerController {

 private final String DEFAULT_ERROR_VIEW = "error/default";

  @ExceptionHandler(Exception.class)
  public ModelAndView defaultErrorHandler(HttpServletRequest req, Exception e) 
  {   
      ModelAndView mav = new ModelAndView();
      mav.addObject("exception", e);
      mav.addObject("danger", e.getMessage());
      mav.addObject("url", req.getRequestURL());
      mav.setViewName(DEFAULT_ERROR_VIEW);
      return mav;
  }
}

